import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { Endpoints } from '@app/enums';
import { Home, LabWork1, LabWork2, LabWork4 } from '@app/containers';
import { Layout } from '@app/components';

const AppRoutes: React.FC = () => {
    return (
        <Routes>
            <Route path="/" element={<Layout />}>
                <Route index element={<Home />} />
                <Route path={Endpoints.LAB_1} element={<LabWork1 />} />
                <Route path={Endpoints.LAB_2} element={<LabWork2 />} />
                <Route path={Endpoints.LAB_4} element={<LabWork4 />} />
            </Route>
        </Routes>
    );
};

export default AppRoutes;
