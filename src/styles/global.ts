import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    *, *:before, *:after {
      box-sizing: border-box
    }

    html,body {
      width: 100%;
      min-height: 100vh;
      margin: 0;
      padding: 0;
    }

    body {
      font-size: ${({ theme }) => `${theme.typography.htmlFontSize}px`};
    }

    h1, h2, h3, h4, h5, h6, p {
        margin: 0;
    }

    .mb {
      &-1 {
        margin-bottom: 0.5rem;
      }

      &-2 {
        margin-bottom: 1rem;
      }

      &-3 {
        margin-bottom: 1.5rem;
      }

      &-4 {
        margin-bottom: 2rem;
      }
    }

    .mr {
      &-1 {
        margin-right: 0.5rem;
      }

      &-2 {
        margin-right: 1rem;
      }

      &-3 {
        margin-right: 1.5rem;
      }

      &-4 {
        margin-right: 2rem;
      }
    }
`;
