export const base = {
    typography: {
        htmlFontSize: 14,
    },
    breakpoints: {
        xs: '600px',
        sm: '960px',
        md: '1280px',
        xl: '1920px',
    },
};
