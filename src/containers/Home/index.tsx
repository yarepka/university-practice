import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Colors } from '@blueprintjs/core';
import { Wrapper } from './styled';
import { Endpoints } from '@app/enums';
import { Flex } from '@app/components/UI';

const Home: React.FC = () => {
    return (
        <Wrapper>
            <Flex column>
                <div className="mb-2">
                    <Link style={{ color: Colors.WHITE, textDecoration: 'none' }} to={Endpoints.LAB_1}>
                        <Card interactive>Лабораторная №1</Card>
                    </Link>
                </div>

                <div className="mb-2">
                    <Link style={{ color: Colors.WHITE, textDecoration: 'none' }} to={Endpoints.LAB_2}>
                        <Card interactive>Лабораторная №2</Card>
                    </Link>
                </div>

                <div>
                    <Link style={{ color: Colors.WHITE, textDecoration: 'none' }} to={Endpoints.LAB_4}>
                        <Card interactive>Лабораторная №4</Card>
                    </Link>
                </div>
            </Flex>
        </Wrapper>
    );
};

export default Home;
