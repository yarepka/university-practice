import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 70%;
    margin: 0 auto;
    height: 100%;
`;
