import React, { useState } from 'react';
import { Button, Card, FormGroup, Intent, TextArea } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task2: React.FC = () => {
    const [arrText, setArrText] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // convert text to array of numbers
        let amountOfNegativeNumbers = 0;
        const arrOfRows = arrText.trim().split('\n');

        for (let i = 0; i < arrOfRows.length; i++) {
            const arr = arrOfRows[i].split(' ');
            const arrNumbers = arr.map((str) => +str);

            for (let j = 0; j < arrNumbers.length; j++) {
                const num = arrNumbers[j];
                if (isNaN(num)) {
                    setIsResultValid(false);
                    setResultText(`Строка ${i} не прошла валидацию, строка должна содержать числа`);
                    return;
                }

                if (num < 0) amountOfNegativeNumbers++;
            }
        }

        setIsResultValid(true);
        setResultText(`Кол-во отрицательных цисел - ${amountOfNegativeNumbers}`);
    };

    const handleClearForm = () => {
        setArrText('');
        setIsResultValid(false);
        setResultText('');
    };

    return (
        <div>
            <h2 className="mb-1">Задание №2</h2>

            <Card className="mb-1">Подсчитать общее количество отрицательных элементов в матрице A(6x9)</Card>

            <form onSubmit={handleSubmit}>
                <div>
                    <FormGroup label="Матрица чисел">
                        <TextArea
                            style={{ minHeight: '15rem' }}
                            placeholder="Введите эл-ты матрицы, эл-ты разделить пробелами, каждая строка на новой линии"
                            value={arrText}
                            onChange={(e) => setArrText(e.target.value)}
                            fill
                        />
                    </FormGroup>

                    <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                        <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                            Получить результат
                        </Button>
                        <Button onClick={handleClearForm} intent={Intent.WARNING}>
                            Очистить форму
                        </Button>
                    </div>

                    {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
                </div>
            </form>
        </div>
    );
};

export default Task2;
