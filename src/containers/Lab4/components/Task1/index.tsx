import React, { useState } from 'react';
import { Button, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task1: React.FC = () => {
    const [arrText, setArrText] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // convert text to array of numbers
        const arr = arrText.trim().split(' ');
        const arrNumbers = arr.map((str) => +str);

        if (arrNumbers.some((num) => isNaN(num))) {
            setIsResultValid(false);
            setResultText('Массив должен содержать цифры');
            return;
        }

        const amountOfNegativeNumbers = arrNumbers.reduce((prevValue, num) => (num < 0 ? prevValue + 1 : prevValue), 0);
        setIsResultValid(true);
        setResultText(`Кол-во отрицательных цисел - ${amountOfNegativeNumbers}`);
    };

    const handleClearForm = () => {
        setArrText('');
        setIsResultValid(false);
        setResultText('');
    };

    return (
        <div>
            <h2 className="mb-1">Задание №1</h2>

            <Card className="mb-1">Подсчитать общее количество отрицательных элементов в массиве D(42)</Card>

            <form onSubmit={handleSubmit}>
                <div>
                    <FormGroup label="Массив чисел">
                        <InputGroup
                            placeholder="Введите эл-ты массива через пробел"
                            value={arrText}
                            onChange={(e) => setArrText(e.target.value)}
                        />
                    </FormGroup>

                    <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                        <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                            Получить результат
                        </Button>
                        <Button onClick={handleClearForm} intent={Intent.WARNING}>
                            Очистить форму
                        </Button>
                    </div>

                    {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
                </div>
            </form>
        </div>
    );
};

export default Task1;
