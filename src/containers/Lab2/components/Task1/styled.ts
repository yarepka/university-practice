import styled from 'styled-components';
import { Card, Colors } from '@blueprintjs/core';

export const ResultCard = styled(Card)<{ $isValid: boolean }>`
    background: ${({ $isValid }) => `${$isValid ? Colors.GREEN2 : Colors.RED1} !important`};
`;
