import React, { useState } from 'react';
import { Button, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task1: React.FC = () => {
    const [m, setM] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const mNumber = parseInt(m);

        // Check If m number
        if (isNaN(mNumber)) {
            setIsResultValid(false);
            setResultText('m должно быть целым числом');
            return;
        }

        if (mNumber < 2) {
            setIsResultValid(false);
            setResultText('m должно быть больше 1');
            return;
        }

        let maxK;
        const kTemp = mNumber / 4;
        const kWihoutDecimalPart = ~~kTemp;

        if (kTemp > kWihoutDecimalPart) {
            maxK = kWihoutDecimalPart;
        } else {
            maxK = kWihoutDecimalPart - 1;
        }

        setIsResultValid(true);
        setResultText(`Наибольшее целое k, при котором 4k < m равно ${maxK}`);
    };

    const handleClearForm = () => {
        setIsResultValid(false);
        setResultText('');
        setM('');
    };

    return (
        <div>
            <h2 className="mb-1">Задание №1</h2>

            <Card className="mb-1">{`Дано целое m>1. Получить наибольшее целое k, при котором 4k<m.`}</Card>

            <form onSubmit={handleSubmit}>
                <div>
                    <FormGroup label="m">
                        <InputGroup
                            value={m}
                            onChange={(e) => setM(e.target.value)}
                            placeholder="Введите целое цисло m"
                        />
                    </FormGroup>

                    <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                        <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                            Получить результат
                        </Button>
                        <Button onClick={handleClearForm} intent={Intent.WARNING}>
                            Очистить форму
                        </Button>
                    </div>

                    {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
                </div>
            </form>
        </div>
    );
};

export default Task1;
