import React, { useState } from 'react';
import { Button, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task3: React.FC = () => {
    const [str, setStr] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // get string before finish point

        // check if finish point exists
        if (str.indexOf('.') === -1) {
            setIsResultValid(false);
            setResultText('Вы забыли добавить точку в конец строки');
            return;
        }

        const splittedStr = str.split('.');
        const strBeforeFinishPoint = splittedStr[0];

        const finalStr = strBeforeFinishPoint.split('').join(' ');
        setIsResultValid(true);
        setResultText(`Измененная строка - ${finalStr}`);
    };

    const handleClearForm = () => {
        setIsResultValid(false);
        setResultText('');
        setStr('');
    };

    return (
        <div>
            <h2 className="mb-1">Задание №3</h2>

            <Card className="mb-1">
                Дана строка символов до точки. Изменить ее таким образом, чтобы все символы отделялись друг от друга
                одним пробелом.
            </Card>

            <form onSubmit={handleSubmit}>
                <div>
                    <FormGroup label="Строка" helperText="Введите строку символов до точки">
                        <InputGroup
                            placeholder="Строка символов"
                            value={str}
                            onChange={(e) => setStr(e.target.value)}
                        />
                    </FormGroup>

                    <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                        <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                            Получить результат
                        </Button>
                        <Button onClick={handleClearForm} intent={Intent.WARNING}>
                            Очистить форму
                        </Button>
                    </div>

                    {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
                </div>
            </form>
        </div>
    );
};

export default Task3;
