import React from 'react';
import { Divider } from '@blueprintjs/core';
import { Wrapper } from './styled';
import { Task1, Task2, Task3 } from './components';

const LabWork1: React.FC = () => {
    return (
        <Wrapper>
            <div className="mb-2">
                <Task1 />
            </div>
            <Divider />
            <div className="mb-2">
                <Task2 />
            </div>
            <Divider />
            <div className="mb-2">
                <Task3 />
            </div>
        </Wrapper>
    );
};

export default LabWork1;
