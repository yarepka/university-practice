import React, { useState } from 'react';
import { Button, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task1: React.FC = () => {
    const [text, setText] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (/^[0-9]+$/.test(text)) {
            setIsResultValid(true);
            setResultText(`Натуральное число состоит из ${text.length} цифр.`);
        } else {
            setIsResultValid(false);
            setResultText(`Введено неверное число. Введеное число должно быть натуральным.`);
        }
    };

    const handleClearForm = () => {
        setText('');
        setIsResultValid(false);
        setResultText('');
    };

    return (
        <div>
            <h2 className="mb-1">Задание №1</h2>

            <Card className="mb-1">
                Дано натуральное число N. Составить программу для определения количества цифр в этом числе.
            </Card>

            <form onSubmit={handleSubmit}>
                <FormGroup className="mb-1" label="Натуральное число">
                    <InputGroup
                        value={text}
                        onChange={(e) => setText(e.target.value)}
                        placeholder="Введите натуральное число"
                    />
                </FormGroup>

                <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                    <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                        Получить результат
                    </Button>
                    <Button onClick={handleClearForm} intent={Intent.WARNING}>
                        Очистить форму
                    </Button>
                </div>

                {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
            </form>
        </div>
    );
};

export default Task1;
