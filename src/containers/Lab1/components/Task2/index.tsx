import React, { useState } from 'react';
import { Card, FormGroup, InputGroup, Intent, Button } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task2: React.FC = () => {
    const [arrText, setArrText] = useState('');
    const [startInterval, setStartInterval] = useState('');
    const [endInterval, setEndInterval] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // convert text to array of numbers
        const arr = arrText.trim().split(' ');
        const arrNumbers = arr.map((str) => +str);

        if (arrNumbers.some((num) => isNaN(num))) {
            setIsResultValid(false);
            setResultText('Массив должен содержать цифры');
            return;
        }

        // get [A, B]
        const startIntervalNumber = parseInt(startInterval);
        const endIntervalNumber = parseInt(endInterval);

        // Check If number
        if (
            [startInterval, endInterval].some((interval) => !/^[0-9]+$/.test(interval)) ||
            isNaN(startIntervalNumber) ||
            isNaN(endIntervalNumber)
        ) {
            setIsResultValid(false);
            setResultText('Начальное и конечные значения интервала должны быть числами');
            return;
        }

        // Check If A < B
        if (startIntervalNumber > endIntervalNumber) {
            setIsResultValid(false);
            setResultText('Начальное значение интервала должно быть меньше конечного значения интервала');
            return;
        }

        const indexArr: number[] = [];
        for (let i = 0; i < arrNumbers.length; i++) {
            if (indexArr.length >= 2) break;

            const num = arrNumbers[i];
            if (num < startIntervalNumber || num > endIntervalNumber) {
                indexArr.push(i);
            }
        }

        if (indexArr.length === 2) {
            const [firstIndex, secondIndex] = indexArr;
            const temp = arrNumbers[firstIndex];
            arrNumbers[firstIndex] = arrNumbers[secondIndex];
            arrNumbers[secondIndex] = temp;

            setIsResultValid(true);
            setResultText(`${arrNumbers.join(' ')}`);
        } else {
            setIsResultValid(false);
            setResultText(
                `Было найдено ${indexArr.length} чисел на интервале [${startIntervalNumber}, ${endIntervalNumber}]`
            );
        }
    };

    const handleClearForm = () => {
        setStartInterval('');
        setEndInterval('');
        setIsResultValid(false);
        setResultText('');
        setArrText('');
    };

    return (
        <div>
            <h2 className="mb-1">Задание №2</h2>

            <Card className="mb-1">
                Найти 2 первых элемента в массиве С (17), значения которых не попадают в заданный с клавиатуры диапазон
                [A, B]. Поменять их местами.
            </Card>

            <form onSubmit={handleSubmit}>
                <div>
                    <FormGroup label="Массив чисел">
                        <InputGroup
                            placeholder="Введите эл-ты массива через пробел"
                            value={arrText}
                            onChange={(e) => setArrText(e.target.value)}
                        />
                    </FormGroup>

                    <div style={{ display: 'flex' }}>
                        <FormGroup style={{ flex: 1 }} className="mr-1" label="А">
                            <InputGroup
                                placeholder="Введите начальную величину диапазона"
                                value={startInterval}
                                onChange={(e) => setStartInterval(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup style={{ flex: 1 }} label="B">
                            <InputGroup
                                placeholder="Введите конечную величину диапазона"
                                value={endInterval}
                                onChange={(e) => setEndInterval(e.target.value)}
                            />
                        </FormGroup>
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                        <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                            Получить результат
                        </Button>
                        <Button onClick={handleClearForm} intent={Intent.WARNING}>
                            Очистить форму
                        </Button>
                    </div>

                    {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
                </div>
            </form>
        </div>
    );
};

export default Task2;
