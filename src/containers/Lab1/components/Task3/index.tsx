import React, { useState } from 'react';
import { Button, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { ResultCard } from './styled';

const Task3: React.FC = () => {
    const [arrText, setArrText] = useState('');
    const [isResultValid, setIsResultValid] = useState(false);
    const [resultText, setResultText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // convert text to array of numbers
        const arr = arrText.trim().split(' ');
        const arrNumbers = arr.map((str) => +str);

        if (arrNumbers.some((num) => isNaN(num))) {
            setIsResultValid(false);
            setResultText('Массив должен содержать цифры');
            return;
        }

        let minValue = arrNumbers[0];
        let minIndex = 0;

        for (let i = 1; i < arrNumbers.length; i++) {
            if (arrNumbers[i] < minValue) {
                minValue = arrNumbers[i];
                minIndex = i;
            }
        }

        setIsResultValid(true);
        setResultText(`Минимум - ${minValue}, местоположение минимума - ${minIndex}`);
    };

    const handleClearForm = () => {
        setIsResultValid(false);
        setResultText('');
        setArrText('');
    };

    return (
        <div>
            <div>
                <h2 className="mb-1">Задание №3</h2>

                <Card className="mb-1">
                    В массиве А (35) найти минимум, определить его местоположение (с учетом возможного повторения).
                </Card>

                <form onSubmit={handleSubmit}>
                    <div>
                        <FormGroup label="Массив чисел">
                            <InputGroup
                                placeholder="Введите эл-ты массива через пробел"
                                value={arrText}
                                onChange={(e) => setArrText(e.target.value)}
                            />
                        </FormGroup>

                        <div style={{ display: 'flex', justifyContent: 'flex-end' }} className="mb-1">
                            <Button className="mr-1" intent={Intent.SUCCESS} type={'submit'}>
                                Получить результат
                            </Button>
                            <Button onClick={handleClearForm} intent={Intent.WARNING}>
                                Очистить форму
                            </Button>
                        </div>

                        {resultText.trim().length > 0 && <ResultCard $isValid={isResultValid}>{resultText}</ResultCard>}
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Task3;
