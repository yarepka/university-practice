import React from 'react';
import styled from 'styled-components';
import { Classes } from '@blueprintjs/core';
import AppRoutes from '@app/routes';
import { RouterProvider, ThemeProvider } from '@app/components/Providers';

const App: React.FC = () => (
    <ThemeProvider>
        <RouterProvider>
            <Wrapper className={`${Classes.DARK}`}>
                <AppRoutes />
            </Wrapper>
        </RouterProvider>
    </ThemeProvider>
);

const Wrapper = styled.div`
    display: flex;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
`;

export default App;
