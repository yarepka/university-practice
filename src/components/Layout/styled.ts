import { Colors } from '@blueprintjs/core';
import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
`;

export const Sidebar = styled.div`
    width: 17rem;
`;

export const Content = styled.div`
    width: 100%;
    padding: 1.5rem;
    flex-grow: 1;
    overflow-y: scroll;
    background: ${Colors.DARK_GRAY5};
`;
