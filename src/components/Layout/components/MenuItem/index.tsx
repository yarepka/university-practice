import React from 'react';
import { Icon, IconName } from '@blueprintjs/core';
import { MenuItemLink } from './styled';

interface MenuItemProps {
    title: string;
    link: string;
    icon?: IconName;
}

type Props = MenuItemProps;

const MenuItem: React.FC<Props> = ({ title, link, icon }) => {
    return (
        <MenuItemLink to={link}>
            {icon && <Icon className="mr-1" icon={icon} />}
            <span>{title}</span>
        </MenuItemLink>
    );
};

export default MenuItem;
