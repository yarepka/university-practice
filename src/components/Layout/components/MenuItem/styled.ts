import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const MenuItemLink = styled(Link)`
    display: flex;
    padding: 1rem 2rem;
    align-items: center;
    cursor: pointer;
    transition: background-color 0.25s;
    color: white !important;
    text-decoration: none !important;

    &:hover {
        background-color: #202b33;
    }

    .i {
        margin-right: 0.7rem;
    }
`;
