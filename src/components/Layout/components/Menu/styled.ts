import { Colors } from '@blueprintjs/core';
import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 1.5rem 0;
    height: 100%;
    background: ${Colors.DARK_GRAY4};
`;

export const Logo = styled.img`
    width: 90%;
    display: block;
    margin: 0px auto;
`;
