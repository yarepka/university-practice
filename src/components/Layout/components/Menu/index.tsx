import React from 'react';
import { IconName } from '@blueprintjs/core';
import { Link } from 'react-router-dom';
import { Wrapper, Logo } from './styled';
import { Endpoints } from '@app/enums';
import { MenuItem } from '../';
import logo from '@app/assets/logo.png';

const menuItems: { title: string; link: string; icon?: IconName }[] = [
    {
        title: 'Лабораторная №1',
        link: Endpoints.LAB_1,
        icon: 'build',
    },
    {
        title: 'Лабораторная №2',
        link: Endpoints.LAB_2,
        icon: 'draw',
    },
    {
        title: 'Лабораторная №4',
        link: Endpoints.LAB_4,
        icon: 'key',
    },
];

const Menu: React.FC = () => {
    return (
        <Wrapper className="sidebar-wrapper">
            <Link style={{ display: 'block' }} className="mb-4" to={'/'}>
                <Logo src={logo} />
            </Link>
            <ul style={{ margin: 0, padding: 0 }}>
                {menuItems.map((props) => (
                    <li key={props.title}>
                        <MenuItem {...props} />
                    </li>
                ))}
            </ul>
        </Wrapper>
    );
};

export default Menu;
