import React from 'react';
import { Outlet } from 'react-router-dom';
import { Menu } from './components';
import { Wrapper, Sidebar, Content } from './styled';

const Layout: React.FC = () => {
    return (
        <Wrapper>
            <Sidebar>
                <Menu />
            </Sidebar>
            <Content>
                <Outlet />
            </Content>
        </Wrapper>
    );
};

export default Layout;
