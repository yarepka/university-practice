import React from 'react';
import { Spinner } from '@blueprintjs/core';
import { Flex } from '@app/components/UI';

const FullscreenLoading: React.FC = () => {
    return (
        <Flex>
            <Spinner />
        </Flex>
    );
};

export default FullscreenLoading;
