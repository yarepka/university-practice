import React from 'react';
import { ThemeProvider as DefThemeProvider } from 'styled-components';
import { base } from '@app/styles/themes';
import { GlobalStyle } from '@app/styles/global';

const ThemeProvider: React.FC = ({ children }) => {
    return (
        <DefThemeProvider theme={base}>
            <GlobalStyle />
            {children}
        </DefThemeProvider>
    );
};

export default ThemeProvider;
