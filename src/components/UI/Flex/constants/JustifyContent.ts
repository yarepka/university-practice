const JustifyContent = {
    FLEX_START: 'flex-start' as const,
    FLEX_END: 'flex-end' as const,
    CENTER: 'center' as const,
    SPACE_BETWEEN: 'space-between' as const,
    SPACE_AROUND: 'space-around' as const,
};

export default JustifyContent;
