const AlignItems = {
    NORMAL: 'normal' as const,
    FLEX_START: 'flex-start' as const,
    FLEX_END: 'flex-end' as const,
    CENTER: 'center' as const,
    START: 'start' as const,
    END: 'end' as const,
    SELF_START: 'self-start' as const,
    SELF_END: 'self-end' as const,
    BASELINE: 'baseline' as const,
    STRETCH: 'stretch' as const,
};

export default AlignItems;
