import styled from 'styled-components';
import { FlexProps } from './types';

export const StyledFlex = styled.div<FlexProps>`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: ${({ column }) => (column ? 'column' : 'row')};
    align-items: ${({ align }) => align};
    justify-content: ${({ justify }) => justify};
`;
