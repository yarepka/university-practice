import React, { HTMLAttributes } from 'react';
import { StyledFlex } from './styled';
import { AlignItems, JustifyContent } from './constants';
import { FlexProps } from './types';

type Props = HTMLAttributes<HTMLDivElement> & FlexProps;

const Flex: React.FC<Props> = ({
    align = AlignItems.CENTER,
    justify = JustifyContent.CENTER,
    column = false,
    ...props
}) => <StyledFlex align={align} justify={justify} column={column} {...props} />;

export default Flex;
