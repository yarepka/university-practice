import { AlignItems } from '../constants';
import { JustifyContent } from '../constants';

type AlignItems = keyof typeof AlignItems;
type JustifyContent = keyof typeof JustifyContent;

export default interface FlexProps {
    column?: boolean;
    align?: AlignItems;
    justify?: JustifyContent;
}
