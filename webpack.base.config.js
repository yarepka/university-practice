const path = require('path');
const Dotenv = require('dotenv-webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const getPlugins = () => {
    return [
        new Dotenv(),
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, 'src/assets/favicon.png'),
                    to: path.resolve(__dirname, 'dist'),
                },
            ],
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
    ];
};

const getLoaders = () => {
    return [
        {
            test: /\.(png|jpe?g|svg|gif)$/,
            type: 'asset/resource',
        },
        {
            test: /\.(ttf|woff|woff2|eot)$/,
            use: ['file-loader'],
        },
    ];
};

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: ['./index.tsx'],
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.react.js'],
        alias: {
            '@app': path.resolve(__dirname, 'src'),
        },
    },
    plugins: getPlugins(),
    module: {
        rules: getLoaders(),
    },
};
