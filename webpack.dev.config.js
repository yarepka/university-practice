const path = require('path');
const { merge } = require('webpack-merge');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const getPlugins = () => {
    return [
        new HTMLWebpackPlugin({
            template: './index.html',
            minify: {
                collapseWhitespace: false,
            },
        }),
        new ESLintPlugin({
            extensions: ['ts', 'tsx'],
        }),
    ];
};

const getLoaders = () => {
    return [
        {
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader, 'cache-loader', 'css-loader'],
        },
        {
            test: /\.s[ac]ss$/,
            use: [MiniCssExtractPlugin.loader, 'cache-loader', 'css-loader', 'sass-loader'],
        },
        {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: [
                'cache-loader',
                {
                    loader: 'babel-loader',
                    options: {
                        transpileOnly: true,
                    },
                },
            ],
        },
        {
            test: /\.ts(x?)$/,
            exclude: /node_modules/,
            use: [
                'cache-loader',
                {
                    loader: 'ts-loader',
                    options: {
                        happyPackMode: true,
                        transpileOnly: true,
                    },
                },
            ],
        },
    ];
};

const baseConfig = require('./webpack.base.config');

const devConfig = merge(baseConfig, {
    mode: 'development',
    output: {
        filename: `[name].js`,
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    devServer: {
        port: 8081,
        open: true,
        hot: false,
        historyApiFallback: true,
    },
    devtool: 'eval-source-map',
    plugins: getPlugins(),
    module: {
        rules: getLoaders(),
    },
});

module.exports = devConfig;
