const path = require('path');
const { merge } = require('webpack-merge');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

const getPlugins = () => {
    return [
        new HTMLWebpackPlugin({
            template: './index.html',
            minify: {
                collapseWhitespace: true,
            },
        }),
    ];
};

const getLoaders = () => {
    return [
        {
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader, 'css-loader'],
        },
        {
            test: /\.s[ac]ss$/,
            use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
        },
        {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'babel-loader',
                    options: {
                        transpileOnly: true,
                    },
                },
            ],
        },
        {
            test: /\.ts(x?)$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'ts-loader',
                    options: {
                        happyPackMode: true,
                        transpileOnly: true,
                    },
                },
            ],
        },
    ];
};

const baseConfig = require('./webpack.base.config');

const prodConfig = merge(baseConfig, {
    mode: 'production',
    output: {
        filename: `[name].[contenthash].js`,
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
        minimizer: [new CssMinimizerPlugin(), new TerserWebpackPlugin({ parallel: true })],
    },
    plugins: getPlugins(),
    module: {
        rules: getLoaders(),
    },
});

module.exports = prodConfig;
